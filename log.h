/*
 * This file is part of adrld.
 *
 * adrld is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * adrld is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with adrld.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <stdlib.h>

#define INFO_ON 1

#define PANIC(fmt, ...) do { \
    fprintf(stderr, fmt, ##__VA_ARGS__); \
    abort(); \
} while (0)

#ifdef DEBUG_ON
#define DEBUG(fmt, ...) do { \
    fprintf(stdout, fmt, ##__VA_ARGS__); \
} while (0)
#else
#define DEBUG(...) do {} while (0)
#endif

#ifdef INFO_ON
#define INFO(fmt, ...) do { \
    fprintf(stdout, fmt, ##__VA_ARGS__); \
} while (0)
#else
#define INFO(...) do {} while (0)
#endif

#endif
