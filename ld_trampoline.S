/*
 * This file is part of adrld.
 *
 * adrld is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * adrld is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with adrld.  If not, see <http://www.gnu.org/licenses/>.
 */
	.text
	.globl _ld_runtime_resolve
	.type _ld_runtime_resolve, #function
	.align 2
_ld_runtime_resolve:

	@ Most of the content is copied from _dl_runtime_resolve.
	@
	@ This is the _ld_runtime_resolve caller.
	@
	@ push	{lr}		; (str lr, [sp, #-4]!)
	@ ldr	lr, [pc, #4]	; c38 <__libc_init@plt-0x4>
	@ add	lr, pc, lr
	@ ldr	pc, [lr, #8]!
	@
	@ We are called with:
	@ stack[0] contains the return address from this call
	@ ip contains &GOT[n+3], pointer to the function
	@ lr points to &GOT[2]

	stmdb sp!, {r0-r4}

	@ This is pointer to elf. We stored it at GOT[1]
	ldr r0, [lr, #-4]

	@ Calculate index into .rel.plt, which is 8 bytes per entry
	sub r1, ip, lr
	sub r1, r1, #4
	lsr r1, r1, #2

	@ Store the func addr here
	mov r2, ip

	bl _ld_fixup

	mov ip, r0
	ldmia sp!, {r0-r4,lr}
	bx ip
	.size _ld_runtime_resolve, .-_ld_runtime_resolve
