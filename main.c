/*
 * This file is part of adrld.
 *
 * adrld is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * adrld is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with adrld.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>

#include "ld.h"

int
main(int argc, char **argv, char **envp)
{
    int ret;

    ret = exec_load(argv[1], argc, argv, envp);
    printf("%s result: %d\n", argv[1], ret);

    return ret;
}
